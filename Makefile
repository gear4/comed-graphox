include deps/platform.mk
include deps/flags.mk

override CFLAGS+= -Isources/shared -Isources/engine -Isources/fpsgame -Isources/mod -Wall -fsigned-char
override CXXFLAGS+= -Isources/shared -Isources/engine -Isources/fpsgame -Isources/mod -std=gnu++0x -Wall -fsigned-char -fno-exceptions -fno-rtti
ifneq (,$(findstring -ggdb,$(CXXFLAGS)))
  STRIP=true
  UPX=true
else
  UPX=upx
endif

CLIENT_OBJS:= \
	sources/shared/crypto.o \
	sources/shared/geom.o \
	sources/shared/stream.o \
	sources/shared/tools.o \
	sources/shared/zip.o \
	sources/engine/3dgui.o \
	sources/engine/bih.o \
	sources/engine/blend.o \
	sources/engine/blob.o \
	sources/engine/client.o	\
	sources/engine/command.o \
	sources/engine/console.o \
	sources/engine/cubeloader.o \
	sources/engine/decal.o \
	sources/engine/dynlight.o \
	sources/engine/glare.o \
	sources/engine/grass.o \
	sources/engine/lightmap.o \
	sources/engine/main.o \
	sources/engine/material.o \
	sources/engine/menus.o \
	sources/engine/movie.o \
	sources/engine/normal.o	\
	sources/engine/octa.o \
	sources/engine/octaedit.o \
	sources/engine/octarender.o \
	sources/engine/physics.o \
	sources/engine/pvs.o \
	sources/engine/rendergl.o \
	sources/engine/rendermodel.o \
	sources/engine/renderparticles.o \
	sources/engine/rendersky.o \
	sources/engine/rendertext.o \
	sources/engine/renderva.o \
	sources/engine/server.o	\
	sources/engine/serverbrowser.o \
	sources/engine/shader.o \
	sources/engine/shadowmap.o \
	sources/engine/sound.o \
	sources/engine/texture.o \
	sources/engine/water.o \
	sources/engine/world.o \
	sources/engine/worldio.o \
	sources/fpsgame/ai.o \
	sources/fpsgame/client.o \
	sources/fpsgame/entities.o \
	sources/fpsgame/fps.o \
	sources/fpsgame/monster.o \
	sources/fpsgame/movable.o \
	sources/fpsgame/render.o \
	sources/fpsgame/scoreboard.o \
	sources/fpsgame/server.o \
	sources/fpsgame/waypoint.o \
	sources/fpsgame/weapon.o \
	sources/mod/irc.o \
	sources/mod/cdemo.o \
	sources/mod/events.o \
	sources/mod/comed.o \
	sources/mod/geoip.o \
	sources/mod/extendedinfo.o \
	sources/mod/translate.o \
	sources/mod/names.o \
	sources/mod/strtool.o \
	sources/mod/mod.o

ifeq ($(ACC),1)
  override CXXFLAGS+= -DACC
  override CLIENT_OBJS+= sources/mod/anticheat.o sources/mod/anticheatparse.o
endif

MACOBJC:= \
	xcode/Launcher.o \
	xcode/main.o
MACOBJCXX:= xcode/macutils.o

ifdef WINDOWS
override LDFLAGS+= -mwindows
override LIBS+= -lGeoIP -lenet -lSDL2 -lSDL2_image -ljpeg -lpng -lz -lSDL2_mixer -logg -lvorbis -lvorbisfile -lws2_32 -lwinmm -lopengl32 -ldxguid -lgdi32 -lole32 -limm32 -lversion -loleaut32 -Wl,-Bstatic -static-libgcc -static-libstdc++ -Wl,-Bstatic -lpthread
endif

ifdef LINUX
override LIBS+= -lGL  -lm -ldl -lGeoIP
ifneq (, $(findstring x86_64,$(PREFIX)))
override LDFLAGS+= -Wl,--wrap=__pow_finite,--wrap=__acosf_finite,--wrap=__log_finite,--wrap=__exp_finite,--wrap=__logf_finite,--wrap=__expf_finite,--wrap=__asin_finite,--wrap=__atan2f_finite,--wrap=__log10f_finite,--wrap=__atan2_finite,--wrap=__acos_finite,--wrap=memcpy
CLIENT_OBJS+= quirks/oldglibc64.o
else
override LDFLAGS+= -Wl,--wrap=__pow_finite
override CLIENT_OBJS+= quirks/oldglibc32.o
endif
endif

ifdef MAC
override LIBS+= -lenet -lSDL2 -lSDL2_image -ljpeg -lpng -lz -lSDL2_mixer -logg -lvorbis -lvorbisfile -framework IOKit -framework Cocoa -framework CoreVideo -framework Carbon -framework CoreAudio -framework OpenGL -framework AudioUnit -lm -ldl
endif


quirks/oldglibc%: override CXXFLAGS += -fno-fast-math

default: all

all: client

objclean:
	-$(RM) -r $(CLIENT_OBJS) $(MACOBJC) $(MACOBJCXX) quirks/*.o sauer_client sauerbraten.exe vcpp/mingw.res

clean:  objclean
	-$(RM) -r graphox/

ifdef WINDOWS
client: $(CLIENT_OBJS)
	$(WINDRES) -I vcpp -i vcpp/mingw.rc -J rc -o vcpp/mingw.res -O coff 
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o sauerbraten.exe vcpp/mingw.res $(CLIENT_OBJS) -Wl,--as-needed -Wl,--start-group $(LIBS) -Wl,--end-group
	$(STRIP) sauerbraten.exe
	-$(UPX) sauerbraten.exe
endif

ifdef MAC
$(MACOBJCXX):
	$(CXX) -c $(CXXFLAGS) -o $@ $(subst .o,.mm,$@)
$(MACOBJC):
	$(CC) -c $(CFLAGS) -o $@ $(subst .o,.m,$@)

client:	$(CLIENT_OBJS) $(MACOBJCXX) $(MACOBJC)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o sauerbraten $(CLIENT_OBJS) $(MACOBJCXX) $(MACOBJC) $(LIBS)
	$(STRIP) sauerbraten
	-$(UPX) sauerbraten
endif

ifdef LINUX
client:	$(CLIENT_OBJS)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o sauer_client $(CLIENT_OBJS) -Wl,--as-needed -Wl,--start-group $(LIBS) -lrt -Wl,--end-group
	$(STRIP) sauer_client
ifneq ($(STRIP),true)
ifneq (, $(findstring x86_64,$(PREFIX)))
	./quirks/remove_symbol_version memcpy@GLIBC_2.2.5
endif
endif
	-$(UPX) sauer_client
endif

# DO NOT DELETE

sources/engine/3dgui.o:		sources/engine/engine.h sources/engine/textedit.h sources/mod/translate.h
sources/engine/bih.o:		sources/engine/engine.h
sources/engine/blend.o:		sources/engine/engine.h
sources/engine/blob.o:		sources/engine/engine.h
sources/engine/client.o:	sources/engine/engine.h
sources/engine/command.o:	sources/engine/engine.h sources/mod/mod.h
sources/engine/console.o:	sources/engine/engine.h sources/engine/sdl2_keymap_extrakeys.h sources/mod/mod.h
sources/engine/cubeloader.o:	sources/engine/engine.h
sources/engine/decal.o:		sources/engine/engine.h
sources/engine/dynlight.o:	sources/engine/engine.h
sources/engine/engine.h:	sources/shared/cube.h sources/engine/world.h sources/engine/octa.h sources/engine/lightmap.h sources/engine/bih.h sources/engine/texture.h sources/engine/model.h sources/engine/varray.h
sources/engine/glare.o:		sources/engine/engine.h sources/engine/rendertarget.h
sources/engine/grass.o:		sources/engine/engine.h
sources/engine/lightmap.o:	sources/engine/engine.h
sources/engine/main.o:		sources/engine/engine.h sources/engine/sdosscripts.h sources/mod/translate.h
sources/engine/master.o:	sources/shared/cube.h
sources/engine/material.o:	sources/engine/engine.h
sources/engine/menus.o:		sources/engine/engine.h
sources/engine/movie.o:		sources/engine/engine.h
sources/engine/normal.o:	sources/engine/engine.h
sources/engine/octa.o:		sources/engine/engine.h
sources/engine/octaedit.o:	sources/engine/engine.h
sources/engine/octarender.o:	sources/engine/engine.h
sources/engine/physics.o:	sources/engine/engine.h sources/engine/mpr.h
sources/engine/pvs.o:		sources/engine/engine.h
sources/engine/rendergl.o:	sources/engine/engine.h sources/engine/varray.h
sources/engine/rendermodel.o:	sources/engine/engine.h sources/engine/ragdoll.h sources/engine/animmodel.h sources/engine/vertmodel.h sources/engine/skelmodel.h sources/engine/md2.h sources/engine/md3.h sources/engine/md5.h sources/engine/obj.h sources/engine/smd.h sources/engine/iqm.h
sources/engine/renderparticles.o: sources/engine/engine.h sources/engine/rendertarget.h sources/engine/depthfx.h sources/engine/explosion.h sources/engine/lensflare.h sources/engine/lightning.h
sources/engine/rendersky.o:	sources/engine/engine.h
sources/engine/rendertext.o:	sources/engine/engine.h
sources/engine/renderva.o:	sources/engine/engine.h
sources/engine/serverbrowser.o:	sources/engine/engine.h
sources/engine/server.o:	sources/engine/engine.h
sources/engine/shader.o:	sources/engine/engine.h
sources/engine/shadowmap.o:	sources/engine/engine.h sources/engine/rendertarget.h
sources/engine/sound.o:		sources/engine/engine.h
sources/engine/texture.o:	sources/engine/engine.h sources/engine/scale.h
sources/engine/water.o:		sources/engine/engine.h
sources/engine/world.o:		sources/engine/engine.h
sources/engine/worldio.o:	sources/engine/engine.h
sources/fpsgame/ai.o:		sources/fpsgame/game.h
sources/fpsgame/client.o:	sources/fpsgame/game.h sources/fpsgame/capture.h sources/fpsgame/ctf.h sources/fpsgame/collect.h sources/mod/cdemo.h sources/mod/mod.h
sources/fpsgame/cdemo.o:	sources/fpsgame/game.h
sources/fpsgame/entities.o:	sources/fpsgame/game.h
sources/fpsgame/fps.o:		sources/fpsgame/game.h sources/mod/comed.h sources/mod/mod.h
sources/fpsgame/game.h:		sources/shared/cube.h sources/fpsgame/ai.h
sources/fpsgame/monster.o:	sources/fpsgame/game.h
sources/fpsgame/movable.o:	sources/fpsgame/game.h
sources/fpsgame/render.o:	sources/fpsgame/game.h
sources/fpsgame/scoreboard.o:	sources/fpsgame/game.h sources/mod/comed.h sources/fpsgame/ctf.h
sources/fpsgame/server.o:	sources/fpsgame/game.h sources/fpsgame/capture.h sources/fpsgame/ctf.h sources/fpsgame/collect.h sources/fpsgame/extinfo.h sources/fpsgame/aiman.h
sources/fpsgame/waypoint.o:	sources/fpsgame/game.h
sources/fpsgame/weapon.o:	sources/fpsgame/game.h
sources/shared/crypto.o:	sources/shared/cube.h
sources/shared/cube.h:		sources/shared/tools.h sources/shared/geom.h sources/shared/ents.h sources/shared/command.h sources/shared/iengine.h sources/shared/igame.h
sources/shared/geom.o:		sources/shared/cube.h
sources/shared/stream.o:	sources/shared/cube.h
sources/shared/tools.o:		sources/shared/cube.h
sources/shared/zip.o:		sources/shared/cube.h

xcode/Launcher.o:	xcode/Launcher.h
xcode/main.o:		xcode/Launcher.h

quirks/oldglibc32.o:	quirks/wrapper.hpp
quirks/oldglibc64.o:	quirks/wrapper.hpp

sources/mod/comed.o:		sources/fpsgame/game.h sources/mod/comed.h
sources/mod/events.o:		sources/fpsgame/game.h sources/mod/events.h
sources/mod/extendedinfo.o:	sources/fpsgame/game.h sources/mod/extendedinfo.h
sources/mod/translate.o:	sources/fpsgame/game.h sources/engine/engine.h sources/mod/translate.h
sources/mod/irc.o:		sources/fpsgame/game.h sources/engine/engine.h sources/mod/irc.h
sources/mod/names.o:		sources/fpsgame/game.h sources/mod/names.h
sources/mod/mod.o:		sources/fpsgame/game.h sources/mod/mod.h
