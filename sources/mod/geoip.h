#include <libGeoIP/GeoIP.h>

namespace mod {
  extern bool useGeoIP;

  namespace gip {
    extern const char *getCountryName(uint ip);
    extern const char *getCountryName(const char *ip);
    extern const char *getCountryCode(uint ip);
    extern const char *getCountryCode(const char *ip);
  }
}
