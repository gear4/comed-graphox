/***********************************************************************
 *  WC-NG - Cube 2: Sauerbraten Modification                           *
 *  Copyright (C) 2014, 2015 by Thomas Poechtrager                     *
 *  t.poechtrager@gmail.com                                            *
 *                                                                     *
 *  This program is free software; you can redistribute it and/or      *
 *  modify it under the terms of the GNU General Public License        *
 *  as published by the Free Software Foundation; either version 2     *
 *  of the License, or (at your option) any later version.             *
 *                                                                     *
 *  This program is distributed in the hope that it will be useful,    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
 *  GNU General Public License for more details.                       *
 *                                                                     *
 *  You should have received a copy of the GNU General Public License  *
 *  along with this program; if not, write to the Free Software        *
 *  Foundation, Inc.,                                                  *
 *  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.      *
 ***********************************************************************/

// GraphOX

#include "translate.h"
#include "tools.h"
namespace mod {
  extern void writeconfig(stream *config);

  namespace srvbrsr {
    extern int srvbrsrsort;
    extern int srvbrsrupsidedown;
  }
  
  namespace network {
    extern void dumppacketbuf(ucharbuf &p);
  }

  namespace con {
    extern int contime;
    extern int contimecol;
    extern int contimefull;
    extern int confading;
  }

  namespace hud {
    extern int skinalpha;
    extern int fszoom;
    extern int killcam;
    extern char *guitheme;
  }

  extern int allowmasterserverscripts;

  extern int rendercommand(int x, int y, int w);
  extern void quit();
}
