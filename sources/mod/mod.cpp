/***********************************************************************
 *  WC-NG - Cube 2: Sauerbraten Modification                           *
 *  Copyright (C) 2014, 2015 by Thomas Poechtrager                     *
 *  t.poechtrager@gmail.com                                            *
 *                                                                     *
 *  This program is free software; you can redistribute it and/or      *
 *  modify it under the terms of the GNU General Public License        *
 *  as published by the Free Software Foundation; either version 2     *
 *  of the License, or (at your option) any later version.             *
 *                                                                     *
 *  This program is distributed in the hope that it will be useful,    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
 *  GNU General Public License for more details.                       *
 *                                                                     *
 *  You should have received a copy of the GNU General Public License  *
 *  along with this program; if not, write to the Free Software        *
 *  Foundation, Inc.,                                                  *
 *  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.      *
 ***********************************************************************/

// GraphOX
#include "game.h"
#include "engine.h"
#include <dirent.h>

extern void norm_quit();
extern void writemodcfg(stream *f);
extern string commandbuf;
extern int commandmillis;
namespace mod {
  // Server browser sorting variables
  // thx to a_teammate (from graphox v1/v2)
  namespace srvbrsr {
    XVARP(srvbrsrsort, "change the sorting of the serverbrowser", 0, 2, 9);
    XVARP(srvbrsrupsidedown, "change the sort-direction of the serverbrowser", 0, 3, 3);
  }
  
  // Network helpers/improvements
  namespace network {
    void dumppacketbuf(ucharbuf &p)
    {
      int i = 0;
      while(!p.overread())
        conoutf("%d: %d", ++i, getint(p));
    }
  }

  // Console variables
  // thx to Quality (Qued) for confading/contimecol
  namespace con {
    XVARP(contime, "show time in consoles", 0, 1, 1);
    XVARP(contimecol, "text-color of contime", 0, 4, 7);
    XVARP(contimefull, "show time in full console", 0, 1, 1);
    XVARP(confading, "smoothly fading-out console lines", 0, 1, 1);
  }
  
  namespace ext {
    XVAR(dbgextinfo, "some console spamming for checking vars", 0, 0, 1);
  }

  // HUD  variables
  // thx to killme (Revelade Revolution) for killcam, a_teammate for theme + skin alpha (graphox v1/v2)
  namespace hud {
    XVARP(skinalpha, "transparency of the guiskin", 0, 70, 255);
    XVARP(fszoom, "rifle-zoom full-screen overlay", 0, 1, 1);
    XVARP(killcam, "spec you killer until you can respawn", 0, 3, 3);

    // GraphOX v1/v2
    XSVARFP(guitheme, "theme of guiskin/overlay", "carbon",
    {
      cleanupguitex();
      defformatstring(themefile)("data/themes/%s/theme.cfg", guitheme);
      execfile(themefile, false);
    })
  }

  // translation service
  namespace i18n {
      extern void reloadlanguage();
      XSVARFP(language, "language of translated menus (not all are)", "en", reloadlanguage());
      XVAR(translationcapture, "debug translation", 0, 0, 1);
  }

  XVARP(allowmasterserverscripts, "it's disabled by default for security reasons!", 0, 0, 1);
  XVAR(dbgtypename, "debug if one type is 'n/a' and report to us", 0, 0, 1);
  XVARP(syntaxify, "colored commandline code!", 0, 1, 1);	
  XVARP(cmdwarnings, "warnings because of e.g. missing/wrong chars or too many args", 0, 1, 1);

  // no category
  // thx to Thomas1 for WC-NG to make these commands possible
  XRVAR(servuptime, "returns the time the current server is online", mod::ext::srv::uptime);
  XRSVAR(servmod, "returns the mod of the current server", mod::ext::srv::mod);

  static inline bool is64bitpointer() {
    return sizeof(void*) == 8;
  }

  static inline const char *getosstring() {
#ifdef WIN32
     return is64bitpointer() ? "win64" : "win32";
#else
#ifdef __linux__
    return is64bitpointer() ? "linux64" : "linux32";
#else
#ifdef __APPLE__
    return is64bitpointer() ? "osx64" : "osx32";
#else
#ifdef __FreeBSD__
    return is64bitpointer() ? "freebsd64" : "freebsd32";
#else
    return is64bitpointer() ? "other64" : "other32";
#endif //BSD
#endif //__APPLE__
#endif //__linux__
#endif //WIN32
  }
  XRSVAR(osstring, "the os string $os$bit", getosstring());

  namespace names {
    extern void savenames(stream *configfile);
  }
  void saveconfig() {
    stream *conf = openutf8file(path("graphox.cfg", true), "w");
    if(!conf) return;

    conf->printf("// automatically written on exit, DO NOT MODIFY\n// modify settings in game, or put settings in %s to override anything\n\n", game::autoexec());

    writemodcfg(conf);
    names::savenames(conf);

    delete conf;
  }
  XICOMMAND(writemodcfg, "save the configurations for the modded vars", "", "", (), saveconfig());

  void quit() {
    saveconfig();
    norm_quit();
  }

  XICOMMAND(quit, "quit without asking", "", "", (), quit());
}
