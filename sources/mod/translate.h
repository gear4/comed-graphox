#ifndef __TRANSLATE_H_
#define __TRANSLATE_H_

namespace mod {
    namespace i18n {
        extern int translationcapture;
        extern char *language;
        extern const char *notranslation;
        extern const char *gettranslation(const char *name);
        extern void reloadlanguage();
    }
}

#endif // __TRANSLATE_H_
