/*
* remod:  commandev.cpp
* date:   2007
* author:   degrave, stormchild
*
* events and events handlers
*/

#include "events.h"

extern char *strreplace(const char *s, const char *oldval, const char *newval);

namespace mod
{
  namespace evt {
    void concatpstring(char** str, int count, ...) {
      size_t len = strlen(*str);
      va_list ap;
      va_start(ap, count);
      for (int i = 0; i < count; i++) {
        const char *s = va_arg(ap, const char*);
        len += strlen(s);
      }
      va_end(ap);

      char *res = newstring(*str, len);
      va_start(ap, count);
      char *p = res + strlen(*str);
      for (int i = 0; i < count; i++) {
        const char *s = va_arg(ap, const char*);
        strcpy(p, s);
        p += strlen(s);
      }
      va_end(ap);

      DELETEA(*str);

      *str = res;
    }

    void concatpstring(char** str, const char *piece) {
      return concatpstring(str, 1, piece);
    }

    evt_param::evt_param() {
      type = 0;
      value = NULL;
    }

    evt_param::~evt_param() {
      if(value) {
        switch(type) {
          case 'i':
            delete value_i;
            break;

          case 's':
            DELETEA(value_s);
            break;

          case 'f':
          case 'd':
            delete value_d;
            break;

          default:
            break;
        }
      }
    }

    event::event() {
      custom = NULL;
      fmt = NULL;
    }

    event::~event() {
      DELETEA(custom);
      DELETEA(fmt);
      evt_param *param;
      while(params.length()) {
        param = params.remove(0);
        delete param;
      }
    }

    vector<event *> events; // events queue
    vector<evt_handler> handlers[NUMEVENTS]; //Event handlers

    // Conver event name to string
    char *event2str(eventType type) {
      if((type >= 0) && (type < NUMEVENTS))
        return newstring(eventNames[type]);
      else
        return newstring("");
    }

    eventType str2event(const char *name) {
      for(int i = 0; i <= NUMEVENTS; i++) {
        if(strcmp(name, eventNames[i]) == 0) return (eventType)i;
      }
      return NUMEVENTS;
    }

    event* storeevent(eventType etype, const char *custom, const char *fmt, va_list vl) {
      event *e = new event;
      e->evt_type = etype;

      e->fmt = newstring(fmt);

      // store params
      if(fmt && fmt[0]) {
        const char *c = fmt;
        while(*c) {
          evt_param *param = new evt_param;
          param->type = *c;
          switch(*c++) {
            case 'i': {
              param->value_i = new int;
              *param->value_i = va_arg(vl, int);
              break;
            }

            case 's': {
              char *s = va_arg(vl, char*);
              param->value_s = newstring(s ? s : "");
              break;
            }

            case 'f':
            case 'd': {
              param->value_d = new double;
              *param->value_d  = va_arg(vl, double);
              break;
            }

            default: {
              conoutf("unknown format parameter \"%c\"", *c);
              va_arg(vl, int);
              break;
            }
          }
          e->params.add(param);
        }
      }
      return e;
    }

    // debug information
    void listparams(vector<evt_param *> &params) {
      evt_param *param;
      loopv(params) {
        param = params[i];
        printf("arg%i(%c)=", i, param->type);
        switch(param->type) {
          case 'i': conoutf("%i", *param->value_i); break;
          case 'f':
          case 'd': conoutf("%f", *param->value_d); break;
          case 's': conoutf("%s", param->value_s); break;
          default: conoutf("unknown");
        }
      }
    }

    void eventinfo(event *ev) {
      conoutf("eventinfo:");
      conoutf("\tevent type: %s", event2str(ev->evt_type));
      conoutf("\tparams(%i)", ev->params.length());
      listparams(ev->params);
    }

    template <typename T> T *getarg(vector<evt_param *> &params) {
      if(params.length()) {
        T *value = new T; // int *value = new int;
        T *tmp;
        evt_param *param = params.remove(0);
        tmp = (T*)param->value;
        *value = *tmp;
        delete param;

        return value;
      }
      return NULL;
    }

    // Spezialization for char*
    template <> char *getarg<char>(vector<evt_param *> &params) {
      if(params.length()) {
        evt_param *param = params.remove(0);
        char *value = newstring(param->value_s);
        delete param;

        return value;
      }
      return NULL;
    }

    void addevent(eventType etype, const char *custom, const char *fmt, va_list vl) {
      event *e;
      if((e = storeevent(etype, NULL, fmt, vl)))
        events.add(e);
    }

    void addevent(event *e) {
      events.add(e);
    }

    //Add script callback to event
    void addhandler(const char *evt_type, const char *callbackcmd) {
      ident *id = getident(callbackcmd);
      if(!id) {conoutf("invalid event alias: %s", callbackcmd); return;}
      if(evt_type && evt_type[0] && callbackcmd && callbackcmd[0]) {
        eventType etype = str2event(evt_type);
        if(etype == NUMEVENTS) return;
        evt_handler eh;
        eh.evt_type = etype;
        eh.evt_cmd = newstring(callbackcmd);
        handlers[etype].add(eh);

        // save to graphox.cfg
        (*id).flags |= IDF_GRAPHOX;
      }
    }

    void delhandler(const char* evt_type, const char *cmd) {
      eventType etype = str2event(evt_type);
      if(etype == NUMEVENTS) return;
      loopv(handlers[etype]) {
        evt_handler &eh = handlers[etype][i];
        if(strcmp(cmd, eh.evt_cmd) == 0) {
          DELETEA(eh.evt_cmd);
          handlers[etype].remove(i);
        }
      }
    }

    bool ishandle(eventType etype) {
      return handlers[etype].length();
    }

    void clearhandlers() {
      loopi(NUMEVENTS)
        handlers[i].shrink(0);
    }

    void triggerEvent(event *ev) {
      if(!ev) return;

      eventType etype = ev->evt_type;
      if(ishandle(etype)) {
        char *evparams = newstring("");

        //Check params
        if(ev->fmt && ev->fmt[0]) {
          //Convert params to string
          const char *c = ev->fmt;
          while(*c) {
            const char* p = NULL;
            switch(*c++) {
              case 'i': {
                int *i = getarg<int>(ev->params);
                concatpstring(&evparams, 2, " ", intstr(*i));
                delete i;
                break;
              }

              case 's': {
                p = getarg<char>(ev->params);
                if(p)
                  concatpstring(&evparams, 2, " ", escapestring(p));
                else
                   concatpstring(&evparams, "\"\"");
                DELETEA(p);
                break;
              }

              case 'f':
              case 'd': {
                double *d = getarg<double>(ev->params);
                concatpstring(&evparams, 2, " ", floatstr(*d));
                delete d;
                break;
              }

              default: {
                //Read and forgot
                int *i = getarg<int>(ev->params);
                delete i;
                break;
              }
            }
          }
        }

        //Process handlers
        loopv(handlers[etype]) {
          evt_handler &eh = handlers[etype][i];
          char *evcmd = newstring(eh.evt_cmd);
          concatpstring(&evcmd, evparams);
          execute(evcmd);
          DELETEA(evcmd);
        }

        DELETEA(evparams);
      }
    }

    // execute event instantly
    void onevent(eventType etype, const char *fmt, ...) {
      if((etype < 0) || (etype >= NUMEVENTS)) return;

      va_list vl;
      va_start(vl, fmt);
      event *e = storeevent(etype, NULL, fmt, vl);
      triggerEvent(e);
      delete e;
      va_end(vl);
    }

    void eventsupdate() {
      event *e;
      if(events.length()){
        while(events.length()) {
          e = events.remove(0);
          triggerEvent(e);
          delete e;
        }
      }
    }

    COMMAND(addhandler, "ss");
    COMMAND(delhandler, "ss");
    COMMAND(clearhandlers, "");
  }
}

