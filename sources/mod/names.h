namespace mod {
  namespace names {
    extern void addName(uint ip, char *name);
    extern void addName(uint ip, int cn);
    extern void addName(const int cn, char *name);
  }
}
