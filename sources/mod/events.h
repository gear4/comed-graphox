#ifndef __COMMANDEV_H__
#define __COMMANDEV_H__

#include "game.h"

enum eventType { ONCOMMANDUNKNOWN = 0,
                 ONCONNECT, ONDEATH, ONDISCONNECT, ONDROPFLAG,
                 ONEDITMODE, ONFRAG, ONGETDEMO, ONGETMAP, ONIMISSION,
                 ONKICK, ONLISTDEMOS, ONMAPSTART, ONMAPVOTE, ONMASTERMODE,
                 ONPING, ONIGNORE, ONNEWMAP, ONPAUSEGAME, ONRESUMEGAME, ONRECORDDEMO, ONRESETFLAG,
                 ONRETURNFLAG, ONSAVEDEMO, ONSAYTEAM, ONSCOREFLAG, ONSETMASTER, ONCHANGETEAM,
                 ONSPAWN, ONSPECTATOR, ONSTOPDEMO, ONSUICIDE,
                 ONSWITCHTEAM, ONTAKEFLAG, ONTEAMKILL, ONTEXT,
                 ONRENAME,
                 IRC_ONMSG, IRC_ONPRIVMSG,
                 CUSTOMEVENT, NUMEVENTS };

static const char * const eventNames[] = {
                 "oncommandunknown",
                 "onconnect", "ondeath", "ondisconnect", "ondropflag",
                 "oneditmode", "onfrag", "ongetdemo", "ongetmap", "onimission",
                 "onkick", "onlistdemos", "onmapstart", "onmapvote", "onmastermode",
                 "onping", "onignore", "onnewmap", "onpausegame", "onresumegame", "onrecorddemo", "onresetflag",
                 "onreturnflag", "onsavedemo", "onsayteam", "onscoreflag", "onsetmaster", "onchangeteam",
                 "onspawn", "onspectator", "onstopdemo", "onsuicide",
                 "onswitchteam", "ontakeflag", "onteamkill", "ontext",
                 "onrename",
                 "irc_onmsg", "irc_onprivmsg",
                 "custom event", "number of events" };

namespace mod
{
  namespace evt {
    // event and its callback
    struct evt_handler
    {
        eventType evt_type;   // type of event
        const char *custom;   // custom named event, null by default
        const char *evt_cmd;  // callback command
    };

    // events queue
    struct evt_param
    {
        char type;       // i - int, s - string, f, d - float;
        union {          // param value
            int *value_i;
            char *value_s;
            double *value_d;
            void *value;
        };

        evt_param();
        ~evt_param();
    };

    struct event
    {
        eventType evt_type;
        const char *custom;
        const char *fmt;
        vector<evt_param *> params;

        event();
        ~event();
    };

    char *event2str(eventType type);
    eventType str2event(const char *name);
    void onevent(eventType etype, const char *fmt, ...);    // execute event
    void eventsupdate();
  }
}
#endif

