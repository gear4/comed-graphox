// Based on https://github.com/inexor-game/code/commit/1e95827e1d26d9f0fa9142b2aacc6a570b5c5443 by "IAmNotHanni"

namespace mod {
  extern int dbgtypename;
  extern int syntaxify;
  extern int cmdwarnings;

  const char *gettypename(const char type)
  {
      switch(type)
      {
          case 's': return "string";
          case 'i': return "integer";
          case 'b': return "boolean";
          case 'f': return "float";
          case 't': return "code-ident";
          case 'e': return "code-block [ ]";
          case 'r': return "ident";
          case '$': return "self-ident";
          case 'N': return "{numargs}";
          case 'D': return "*keydown*";
          case 'V': return "code-arguments";
          case 'C': return "char..";
          case '1': return "1";
          case '2': return "2";
          case '3': return "3";
          case '4': return "4";
      }
      if(dbgtypename) conoutf("%s: unknown type %c", __FUNCTION__, type);
      return "n/a";
  }

  string commandlinestatus;

  #define SYNTAXSTRLEN 260*100
  char syntaxline[SYNTAXSTRLEN];
  int currentarg = 0;
  const char *colors_ = "0123456789abcdefghijklmnopqrstTuvwxyz";
  int arglimit = -1;

  const char *syntaxstring(const char *str, int x, int y)
  {
      if(!syntaxify) return str;

      currentarg = -1;

      copystring(syntaxline, "");

      // General info
      bool openstring = false,
           nextisescaped = false,
           nextiscolor = false,
           wrongcolor = false;
      int opencontainer[2] = { 0, 0 },
          wrongcolors = 0,

      // Warnings
          openstringpos = 0,
          escapedpos = 0,
          colorpos = 0;
          
      vector<int> wrongcolorpos,
                  opencontainerpos0,
                  opencontainerpos1;

      for(const char *s = str; *s; s++) switch(*s)
      {
          // '^': Escape next char. Can also be escaped
          case '^':
          {
              if(nextisescaped || nextiscolor) goto def;
              concatstring(syntaxline, tempformatstring("\fs\f5%c\f9", *s), SYNTAXSTRLEN);
              escapedpos = text_widthf(syntaxline);
              nextisescaped = true;
              break;
          }

          // '"': A string. Can be escaped.
          case '"':
          {
              if(nextisescaped || nextiscolor) goto def;
              if(!openstring) openstringpos = text_widthf(syntaxline);
              else openstringpos = 0;
              concatstring(syntaxline, tempformatstring("\f5%c%s", *s, openstring ? "\f7" : "\f4"), SYNTAXSTRLEN);
              openstring = !openstring;
              break;
          }

          // '[' or '(': open container. save calldepth for each of them.
          case '[': case ']': case '(': case ')':
          {
              if(nextisescaped || nextiscolor || openstring) goto def;
              int rnd = *s == '(' || *s == ')' ? 0 : 1,
                  opn = *s == '[' || *s == '(' ? 1 : 0;

              concatstring(syntaxline, tempformatstring("\f3%c", *s), SYNTAXSTRLEN);

              opencontainer[rnd] += (opn ? 1 : -1);
              int col = opencontainer[rnd];
              if(col < 0) col = -col;
              while(col > 2) col -= 3;
              concatstring(syntaxline, tempformatstring("\f%d", col), SYNTAXSTRLEN);
              
              if((opn && opencontainer[rnd] < 0) || (!opn && opencontainer[rnd] > -1))
              {
                  if(rnd == 0) opencontainerpos0.remove(opn ? -opencontainer[rnd] : opencontainer[rnd]);
                  else opencontainerpos1.remove(opn ? -opencontainer[rnd] : opencontainer[rnd]);
              }
              else
              {
                  if(rnd == 0) opencontainerpos0.add(text_widthf(syntaxline)-text_widthf(tempformatstring("%c", *s)));
                  else opencontainerpos1.add(text_widthf(syntaxline)-text_widthf(tempformatstring("%c", *s)));
              }
              break;
          }

          // 'f': the next char defines the color
          case 'f':
          {
              if(!nextisescaped || nextiscolor) goto def;
              escapedpos = 0;
              colorpos = text_widthf(syntaxline);
              concatstring(syntaxline, tempformatstring("%c", *s), SYNTAXSTRLEN);
              nextiscolor = true;
              nextisescaped = false;
              break;
          }

          // special char ';'
          case ';':
              concatstring(syntaxline, tempformatstring("\fs\f3%c\fr", *s), SYNTAXSTRLEN);
              break;

          case ' ':
              if(!opencontainer[0] && !opencontainer[1] && !openstring) currentarg++;
              goto def;

          // default: write any char, check for "nextiscolor" and "nextisescaped"
          default:
          {
          def:
              escapedpos = 0;
              if(nextiscolor)
              {
                  colorpos = 0;
                  nextiscolor = false;
                  wrongcolor = strstr(colors_, tempformatstring("%c", *s)) ? 0 : 1;
                  if(!wrongcolor)
                  {
                      concatstring(syntaxline, tempformatstring("\f%c%c", *s, *s), SYNTAXSTRLEN);
                      break;
                  }
                  wrongcolorpos.add(text_widthf(syntaxline));
                  wrongcolors++;
              }
              concatstring(syntaxline, tempformatstring("%c", *s), SYNTAXSTRLEN);
              if(nextisescaped)
              {
                  concatstring(syntaxline, "\fr", SYNTAXSTRLEN);
                  nextisescaped = false;
              }
          }
      }

      vector<const char *> warnings;
      loopi(2)
      {
          if(opencontainer[i] > 0) warnings.add(tempformatstring("%d too many '%s'", opencontainer[i], i ? "[" : "("));
          else if(opencontainer[i] < 0) warnings.add(tempformatstring("%d too many '%s'", -opencontainer[i], i ? "]" : ")"));
      }
      if(openstring) warnings.add("One '\"'-open without close");
      if(wrongcolors) warnings.add(tempformatstring("There %s '^f'%s with a non-color char following", wrongcolors == 1 ? "is an" : tempformatstring("are %d", wrongcolors), wrongcolors > 1 ? "s" : ""));
      if(nextiscolor) warnings.add("The next char is a color");
      if(nextisescaped) warnings.add("The next char is escaped");

      mod::strtool warn;
      loopv(warnings)
      {
          if(i) warn.add('\n');
          warn.append(warnings[i]);
      }
      copystring(commandlinestatus, warn.str());

      if(openstringpos) draw_textf("\f3_", x+5+openstringpos, y);
      if(escapedpos) draw_textf("\f2_", x+5+escapedpos, y);
      if(colorpos) draw_textf("\f2_", x+5+colorpos, y);
      loopv(wrongcolorpos) draw_textf("\f3_", x+5+wrongcolorpos[i], y);
      loopv(opencontainerpos0) draw_textf("\f0_", x+5+opencontainerpos0[i], y);
      loopv(opencontainerpos1) draw_textf("\f0_", x+5+opencontainerpos1[i], y);

      return syntaxline;
  }

  void toomanyargs(int toomany)
  {
      if(arglimit < 0) return;
      
      if(commandlinestatus[0])
          concatstring(commandlinestatus, tempformatstring("\n%d argument%s too many", toomany, toomany > 1 ? "s" : ""));
      else copystring(commandlinestatus, tempformatstring("%d argument%s too many", toomany, toomany > 1 ? "s" : ""));
  }

  void drawcommandlinestatus(int x, int y)
  {
      if(!strlen(commandlinestatus) || !cmdwarnings) return;
      
      concatstring(commandlinestatus, "\nfor more info type '/conass'");
      int posx = x+50, posy = y - FONTH/2,
          tw, th;
      text_bounds(commandlinestatus, tw, th);
      drawacoloredquad(posx-10, posy-th-10, tw+10, th+10, 245, 155, 35, 0xFF);
      draw_text(commandlinestatus, posx, posy-th, 240, 55, 165, 0xFF);
  }

  int stringfreespaces(const char *str)
  {
      int spaces = 0;
      for(const char *s = str; *s; s++) if(*s == ' ') spaces++;
      return spaces;
  }

  void getarglimit(const char *args)
  {
      arglimit = 0;
      
      loopi(strlen(args))
      {
          switch(args[i])
          {
              case 's': case 'i': case 'b':
              case 'f': case 't': case 'e':
              case 'r':
                  arglimit++; break;
              case '$': case 'N': 
                  break;
              case 'D': case 'V': case 'C':
                  arglimit = -1; return;
              case '1': arglimit += 1; break;
              case '2': arglimit += 2; break;
              case '3': arglimit += 3; break;
              case '4': arglimit += 4; break;
              default: 
                  arglimit++; break;
          }
      }
  }

  bool renderingsuggestion = false;

  int rendercommand(int x, int y, int w)
  {
      if(commandmillis < 0) return 0;
      
      static string suggestion;
      static ullong lasttime = 0;
      int hoffset = 0,
          toomany = 0;
      
      if(SDL_GetTicks()-lasttime > 500) copystring(suggestion, "");
      
      if(commandbuf[0] == '/')
      {
          defformatstring(spitthis)(commandbuf);
          
          vector<char *> commandparts;
          
          char *delimiter = (char *)" ",
               *ptr = strtok(spitthis, delimiter);
          
          while(ptr != NULL)
          {
              commandparts.add(ptr);
              ptr = strtok(NULL, delimiter);
          }
          
          commandparts[0]++;
          ident *id = idents.access(commandparts[0]);
          if(id)
          {
              y -= 2*FONTH;
              hoffset += 2*FONTH;
              
              renderingsuggestion = false;
              
              copystring(suggestion, "");
              
              if(strcmp(id->desc, "no help available"))
                  draw_text(id->desc, x+2*FONTW, id->flags&IDF_READONLY && id->type != ID_COMMAND ? y+FONTH*2 : y+FONTH, 0xFF, 0xFF, 0xFF, 0xFF, -1, w);
              else
              {
                  y += FONTH;
                  hoffset -= FONTH;
              }
              
              switch(id->type) {
                  case ID_FVAR: case ID_VAR: {
                      string minmaxcurrent;
                      switch(id->type)
                      {
                          case ID_FVAR:
                              formatstring(minmaxcurrent)("\f6min: \f0%.3ff\f6 max: \f3%.3ff\f6 current: \f1%.3f", id->minvalf, id->maxvalf, *id->storage.f);
                              break;
                          case ID_VAR:
                              formatstring(minmaxcurrent)("\f6min: \f0%d\f6 max: \f3%d\f6 current: \f1%d", id->minval, id->maxval, *id->storage.i);
                              break;
                      }

                      draw_text(minmaxcurrent, x+2*FONTH, y, 0xFF, 0xFF, 0xFF, 0xFF, -1, w);

                      if(id->flags&IDF_READONLY)
                          draw_text("this var is read-only!", x+2*FONTH, y+FONTH, 0xBB, 0, 0, 0xFF, -1, w);

                      if(currentarg > 1) toomany = currentarg-1;
                  } break;
                  
                  case ID_SVAR: {
                      string current;
                      formatstring(current)("\f6current: \f7\"\f1%s\f7\"", *id->storage.s);

                      draw_text(current, x+2*FONTH, y, 0xFF, 0xFF, 0xFF, 0xFF, -1, w);

                      if(id->flags&IDF_READONLY)
                          draw_text("this var is read-only!", x+2*FONTH, y+FONTH, 0xBB, 0, 0, 0xFF, -1, w);

                      if(currentarg > 1) toomany = currentarg-1;
                  } break;
                  
                  case ID_COMMAND: {
                      getarglimit(id->args);
                      if(currentarg > arglimit)
                          toomany = currentarg-arglimit;
                      
                      if(id->params)
                      {
                          if(strlen(id->params))
                          {
                              defformatstring(copiedargumentlist)(id->params);
                              vector<char *> unpackedparams;

                              char *delimiter = (char *)",",
                                   *tokpointer = strtok(copiedargumentlist, delimiter);

                              while(NULL != tokpointer)
                              {
                                  unpackedparams.add(tokpointer);
                                  tokpointer = strtok(NULL, delimiter);
                              }

                              defformatstring(parameterlist)("\f8usage: \f7/\fs\f3%s\fr", commandparts[0]);
                              loopi(unpackedparams.length())
                                  formatstring(parameterlist)("%s \fs\f%s%s\fr[\fs\f%s%s\fr]",
                                                   parameterlist,
                                                   currentarg-1 == i ? "n" : (stringfreespaces(commandbuf)==i && !strcmp(commandbuf, suggestion) ? "3" : "t"),
                                                   unpackedparams[i],
                                                   currentarg-1 == i ? "n" : "4",
                                                   gettypename(id->args[i])
                                               );
                                  
                              draw_text(parameterlist, x+2*FONTW, y, 0xFF, 0xFF, 0xFF, 0xFF, -1, w);
                          }
                          else
                          {
                              defformatstring(parameterlist)("\f4usage: \f7/\fs\f3%s\fr", commandparts[0]);
                              loopi(id->numargs)
                                  formatstring(parameterlist)("%s \fs\f%s%s\fr",
                                                   parameterlist,
                                                   currentarg-1 == i ? "n" : "5",
                                                   gettypename(id->args[i])
                                               );
                              draw_text(parameterlist, x+2*FONTW, y, 0xFF, 0xFF, 0xFF, 0xFF, -1, w);
                          }
                      }
                      else
                      {
                          defformatstring(parameterlist)("\f4usage: \f7/\f3%s\fr", commandparts[0]);
                          draw_text(parameterlist, x+2*FONTH, y, 0xFF, 0xFF, 0xFF, 0xFF, -1, w);
                      }
                  } break;
              
                  default: {
                      y += FONTH;
                      hoffset -= FONTH;
                  } break;
              }
          }
          else
          {
              static string lastinput;
              renderingsuggestion = true;

              if(SDL_GetTicks()-lasttime > 500 || strcmp(commandparts[0], lastinput))
              {
                  int tries = 0;

                  nosuggestion:
                  formatstring(suggestion)(commandparts[0]);
                  complete(suggestion, NULL);
                  tries++;

                  if((!strcmp(suggestion, commandparts[0]) || !strlen(suggestion)) && !tries) goto nosuggestion;

                  lasttime = SDL_GetTicks();
                  formatstring(lastinput)(commandparts[0]);
              }
          }
      }

      defformatstring(s)("%s %s", commandprompt ? commandprompt : ">", commandbuf);
      int width, height;
      text_bounds(s, width, height, w);
      y -= height;
      if(strlen(suggestion) > strlen(commandbuf)-1 && !stringfreespaces(commandbuf)) draw_text(suggestion, x+text_widthf("> /"), y, 0x00, 0xBB, 0xBB, 0xFF, -1, w);
      draw_text(s, x, y, 0xFF, 0xFF, 0xFF, 0xFF, (commandpos>=0) ? (commandpos+1+(commandprompt?strlen(commandprompt):1)) : strlen(s), w);
      if(commandbuf[0] == '/')
      {
          draw_text(syntaxstring(s, x, y), x, y, 0xFF, 0xFF, 0xFF, 0xFF);
          if(toomany) toomanyargs(toomany);
          drawcommandlinestatus(x, y);
      }

      return height+hoffset;
  }
}
