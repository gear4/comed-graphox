#include "translate.h"
#include "engine.h"

namespace mod {
    namespace i18n {
        struct translation
        {
            char *key;
            char *value;

            translation(const char *key, const char *value) : key(newstring(key)), value(newstring(value)) {}

            ~translation()
            {
                DELETEA(key);
                DELETEA(value);
            }
        };

        hashtable<const char *, translation *> strings;
        const char *notranslation = "No translation available.";

        const char *gettranslation(const char *name)
        {
            translation **c = strings.access(name);
            if(!c)
            {
                if(translationcapture)
                {
                    translation *trans = new translation(name, name);
                    strings[trans->key] = trans;
                    printf("\"%s\" \"%s\"\n", name, name);
                }

                return notranslation;
            }
            return (*c)->value;
        }

        ICOMMAND(gettrans, "s", (const char *name),
        {
            const char *translation = gettranslation(name);
            if(translation == notranslation)
            {
                defformatstring(_translation)("No translation available for: %s.", name);
                return result(_translation);
            }
            return result(translation);
        });

        ICOMMAND(translate, "sss", (const char *stringlanguage, const char *name, const char *string),
        {
            if(strcmp(language, stringlanguage))
                return;
            translation *trans = new translation(name, string);
            strings[trans->key] = trans;
        });

        void reloadlanguage() {
            defformatstring(translationfile)("data/lang/%s.cfg", language);
            execfile(translationfile);
        };
        COMMAND(reloadlanguage, "");
    }
}

