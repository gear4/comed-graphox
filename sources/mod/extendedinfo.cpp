/***********************************************************************
 *  WC-NG - Cube 2: Sauerbraten Modification                           *
 *  Copyright (C) 2014, 2015 by Thomas Poechtrager                     *
 *  t.poechtrager@gmail.com                                            *
 *                                                                     *
 *  This program is free software; you can redistribute it and/or      *
 *  modify it under the terms of the GNU General Public License        *
 *  as published by the Free Software Foundation; either version 2     *
 *  of the License, or (at your option) any later version.             *
 *                                                                     *
 *  This program is distributed in the hope that it will be useful,    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
 *  GNU General Public License for more details.                       *
 *                                                                     *
 *  You should have received a copy of the GNU General Public License  *
 *  along with this program; if not, write to the Free Software        *
 *  Foundation, Inc.,                                                  *
 *  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.      *
 ***********************************************************************/

#include "game.h"
#include "names.h"

// modified from QuEd 1 and WC-NG
// added:
// - server mod information (WC-NG)
// - server uptime
// changed:
// - renamed "extplayerhelper" to "exthelper"
// - added switch in helper function to allow server info to be read upon connection
// removed:
// - extserverbrowser (QuEd)
// - extserverinfo (playercount, desc, etc) (QuEd)

#define EXT_ACK                         -1
#define EXT_NO_ERROR                    0
#define EXT_PLAYERSTATS_RESP_STATS      -11
#define EXT_UPTIME                      0
#define EXT_PLAYERSTATS                 1
#define EXT_EXTENDED_PLAYERSTATS        0xC8343F2
#define EXT_TEAMSCORE                   2
#define EXT_VERSION                     105
#define EXT_VERSION_MIN                 104

namespace mod
{
    namespace ext
    {
        extern int dbgextinfo;
        
        namespace srv
        {
            bool hasmod = false;
            bool sentips = false; // there are already serv-mods that send no ip
            int modident = 0, uptime = 0;
            const char *mod = "default/unknown", *modver, *rev, *build, *uptimestr;
        }

        ENetSocket extinfosock = ENET_SOCKET_NULL;

        ENetSocket getextsock()
        {
            if(extinfosock != ENET_SOCKET_NULL) return extinfosock;
            
            extinfosock = enet_socket_create(ENET_SOCKET_TYPE_DATAGRAM);
            enet_socket_set_option(extinfosock, ENET_SOCKOPT_NONBLOCK, 1);
            enet_socket_set_option(extinfosock, ENET_SOCKOPT_BROADCAST, 1);
            return extinfosock;
        }

        void clientreq(int cn)
        {
            const ENetAddress *paddress = connectedpeer();
            if(!paddress) return;
            
            ENetAddress address = *paddress;
            ENetSocket extsock = getextsock();
            if(extsock == ENET_SOCKET_NULL) return;
            
            address.port = server::serverinfoport(address.port);
            ENetBuffer buf;
            uchar send[MAXTRANS];
            ucharbuf p(send, MAXTRANS);
            putint(p, 0);
            putint(p, EXT_PLAYERSTATS);
            putint(p, cn);
            buf.data = send;
            buf.dataLength = p.length();
            enet_socket_send(extsock, &address, &buf, 1);
        }

        void servreq()
        {
            const ENetAddress *paddress = connectedpeer();
            if(!paddress) return;
            
            ENetAddress address = *paddress;
            ENetSocket extsock = getextsock();
            if(extsock == ENET_SOCKET_NULL) return;
            
            address.port = server::serverinfoport(address.port);
            ENetBuffer buf;
            uchar send[MAXTRANS];
            ucharbuf p(send, MAXTRANS);
            putint(p, 0);
            putint(p, EXT_UPTIME);
            putint(p, 1);                           // request server mod
            buf.data = send;
            buf.dataLength = p.length();
            enet_socket_send(extsock, &address, &buf, 1);
        }

        int exthelper(ucharbuf p, struct extplayerdata &data)
        {
            getint(p);

            int type = getint(p); int otype = type;
            if(type >= 100+EXT_UPTIME && type <= 100+EXT_PLAYERSTATS)
                type -= 100;

            switch(type) 
            {
                case EXT_PLAYERSTATS:
                {
                    srv::sentips = false;
                    char strdata[MAXTRANS];

                    getint(p);
                    if(getint(p) != EXT_ACK || getint(p) != EXT_VERSION) return -2;
                    int err = getint(p);
                    if(err) return -3;

                    if(getint(p) == EXT_PLAYERSTATS_RESP_STATS)
                    {
                        data.cn = getint(p);
                        data.ping = getint(p);
                        getstring(strdata, p);
                        strncpy(data.name, strdata, MAXEXTNAMELENGHT-1);
                        data.name[MAXEXTNAMELENGHT-1] = 0;
                        getstring(strdata, p);
                        strncpy(data.team, strdata, MAXEXTTEAMLENGHT-1);
                        data.team[MAXEXTTEAMLENGHT-1] = 0;
                        data.frags = getint(p);
                        data.flags = getint(p);
                        data.deaths = getint(p);
                        data.teamkills = getint(p);
                        data.acc = getint(p);
                        data.health = getint(p);
                        data.armour = getint(p);
                        data.gunselect = getint(p);
                        data.privilege = getint(p);
                        data.state = getint(p);

                        p.get((uchar*)&data.ip, 3);
                    }

                    if(data.ip) srv::sentips = true;
                    return 0;
                }
                
                case EXT_UPTIME:
                {
                    if(p.get() != 1) return -6;
                    if(getint(p) != EXT_ACK) return -6;
                    int version = getint(p);
                    if(version < EXT_VERSION_MIN || version > EXT_VERSION) return -6;
                    if(!p.remaining()) return -6;

                    srv::uptime = getint(p);
                    int servermod = SRV_DEFAULT;
                    const char *modname = NULL;
                    srv::hasmod = false;
                    if(p.remaining()) 
                    {
                        srv::hasmod = true;
                        servermod = getint(p);
                        if(otype == 100+EXT_UPTIME && servermod == SRV_HOPMOD) servermod = SRV_ZEROMOD; // zeromod
                        uint i = (servermod+2)*-1;
                        if (i < sizeofarray(SERVERMODNAMES)) modname = SERVERMODNAMES[i];

                        srv::modident = servermod;
                        srv::mod = modname;
                        
                        if(p.remaining())
                        {
                            srv::modver = tempformatstring("%d", getint(p));
                            srv::rev = tempformatstring("%d", getint(p));
                            
                            char strdata[MAXTRANS];
                            getstring(strdata, p);
                            
                            srv::build = (const char *)strdata;
                            
                            if(dbgextinfo) 
                                conoutf("mod: %d [%s], ver: %s, rev: %s, build: %s", srv::modident, srv::mod, srv::modver, srv::mod, srv::build);
                        }
                    }
                }
            }
            return -1;
        }

        void processextinfo()
        {
            const ENetAddress *paddress = connectedpeer();
            if(!paddress) return;
            ENetAddress connectedaddress = *paddress;
            ENetSocket extsock = getextsock();
            if(extsock == ENET_SOCKET_NULL) return;
            enet_uint32 events = ENET_SOCKET_WAIT_RECEIVE;
            int s = 0;
            ENetBuffer buf;
            ENetAddress address;
            uchar data[MAXTRANS];
            buf.data = data;
            buf.dataLength = sizeof(data);
            while((s = enet_socket_wait(extsock, &events, 0)) >= 0 && events)
            {
                int len = enet_socket_receive(extsock, &address, &buf, 1);
                if(len <= 0 || connectedaddress.host != address.host ||
                   server::serverinfoport(connectedaddress.port) != address.port) continue;
                ucharbuf p(data, len);
                struct extplayerdata extpdata;
                if(exthelper(p, extpdata) == 0)
                {
                    fpsent *d = game::getclient(extpdata.cn);
                    if(!d || d->extdata.isfinal()) continue;
                    d->extdata.setextplayerinfo(extpdata);
                    if(extpdata.ip) names::addName(extpdata.ip, extpdata.cn);
                    if(!d->extdatawasinit)
                    {
                        d->deaths = extpdata.deaths;
                        d->extdatawasinit = true;
                    }
                }
            }
        }

        int lastcheck = 0;
        void checkextinfos()
        {
            const ENetAddress *paddress = connectedpeer();
            if(!paddress) return;
            processextinfo();
            loopv(game::players)
            {
                fpsent *d = game::players[i];
                if(!d) continue;
                if(d->extdata.needretry())
                {
                    d->extdata.addattempt();
                    clientreq(d->clientnum);
                }
            }
            if(game::player1->extdata.needretry())
            {
                game::player1->extdata.addattempt();
                clientreq(game::player1->clientnum);
            }
            
            
            if(totalmillis-lastcheck > 3000)
            {
                servreq();
                lastcheck = totalmillis;
            }
        }
    } // ext
} // mod
