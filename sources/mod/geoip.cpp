#include "game.h"
#include "geoip.h"

namespace mod {
  static GeoIP * geoip = NULL;
  bool useGeoIP = false;

  namespace gip {
    const char *getCountryName(uint ip) {
      if(!useGeoIP) {conoutf("GeoIP is \f3disabled\f7. Please fix the problem (GeoIP.dat not detected)"); return "";}
      return GeoIP_country_name_by_ipnum(geoip, endianswap(ip));
    };

    const char *getCountryName(const char *ip) {
      if(!useGeoIP) {conoutf("GeoIP is \f3disabled\f7. Please fix the problem (GeoIP.dat not detected)"); return "";}
      return GeoIP_country_name_by_name(geoip, ip);
    };

    const char *getCountryCode(uint ip) {
      if(!useGeoIP) {conoutf("GeoIP is \f3disabled\f7. Please fix the problem (GeoIP.dat not detected)"); return "";}
      return GeoIP_country_code_by_ipnum(geoip, endianswap(ip));
    };

    const char *getCountryCode(const char *ip) {
      if(!useGeoIP) {conoutf("GeoIP is \f3disabled\f7. Please fix the problem (GeoIP.dat not detected)"); return "";}
      return GeoIP_country_code_by_name(geoip, ip);
    };

    void ginit(const char *filename) {
      geoip = GeoIP_open(filename, GEOIP_STANDARD | GEOIP_MEMORY_CACHE);
      if(geoip==NULL) conoutf("\f3Error: could not locate %s\n\tGeoIP disabled", filename);
      else {
        useGeoIP = true;
        conoutf("\f0GeoIP loaded successfully");
      }
    }
    COMMAND(ginit, "s");
  }
}
