#ifndef __ACC_H__
#define __ACC_H__

#define crowd(fmt, ...) conoutf("\f0[Crowd] \f2" fmt "", ##__VA_ARGS__)
namespace mod {
  namespace acc {
    #define PROTOCOL_NUM (9)
    
    enum {
      ACC_DISC_NONE = 0,
      ACC_DISC_EOP,
      ACC_DISC_MSGERR,
      ACC_DISC_TIMEOUT,
      ACC_DISC_OVERFLOW,
      ACC_DISC_PROTOCOL
    };
    
    enum { ST_EMPTY, ST_LOCAL, ST_TCPIP };
    
    enum {
      ACC_N_CONNECT = 0,
      ACC_N_ERROR,
      ACC_N_SERVINFO,
      ACC_N_GUNSELECT,
      ACC_N_SOUND,
      ACC_N_EDITMODE,
      ACC_N_NEWGAME,
      ACC_N_SERVCONN
    };
    
    extern void accDeInit();
    extern void accInit();
    extern void accMainLoop();
    extern void serverConnect(unsigned int, int);
  }
}
#endif