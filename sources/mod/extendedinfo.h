/***********************************************************************
 *  WC-NG - Cube 2: Sauerbraten Modification                           *
 *  Copyright (C) 2014, 2015 by Thomas Poechtrager                     *
 *  t.poechtrager@gmail.com                                            *
 *                                                                     *
 *  This program is free software; you can redistribute it and/or      *
 *  modify it under the terms of the GNU General Public License        *
 *  as published by the Free Software Foundation; either version 2     *
 *  of the License, or (at your option) any later version.             *
 *                                                                     *
 *  This program is distributed in the hope that it will be useful,    *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of     *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the      *
 *  GNU General Public License for more details.                       *
 *                                                                     *
 *  You should have received a copy of the GNU General Public License  *
 *  along with this program; if not, write to the Free Software        *
 *  Foundation, Inc.,                                                  *
 *  51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.      *
 ***********************************************************************/

#ifndef __EXTENDEDINFO_H_
#define __EXTENDEDINFO_H_

#define sizeofarray(a) (sizeof(a)/sizeof(*a))

namespace mod
{
    namespace ext
    {
        namespace srv
        {
            extern bool hasmod, sentips;
            extern int modident, uptime;
            extern const char *mod, *modver, *rev, *build, *uptimestr;
        }

        constexpr const char *SERVERMODNAMES[] =
        {
            "hopmod", "oomod", "spaghettimod",
            "suckerserv", "remod", "noobmod",
            "zeromod"
        };
        
        enum 
        {
            SRV_DEFAULT   = 0,
            SRV_HOPMOD    = -2,
            SRV_OOMOD     = -3,
            SRV_SPAGHETTI = -4,
            SRV_SUCKER    = -5,
            SRV_REMOD     = -6,
            SRV_NOOBMOD   = -7,
            SRV_ZEROMOD   = -8
        };

        extern void clientreq(int cn);
        extern void servreq();

        extern void checkextinfos();
        extern void cleangameinfo(void *p);
    }
}

#define MAXEXTNAMELENGHT 16
#define MAXEXTTEAMLENGHT 5

struct extplayerdata
{
    int cn;
    int ping;
    char name[MAXEXTNAMELENGHT];
    char team[MAXEXTTEAMLENGHT];
    int frags;
    int flags;
    int deaths;
    int teamkills;
    int acc;
    int health;
    int armour;
    int gunselect;
    int privilege;
    int state;
    uint ip;
    int lastseen;
    int mod;
    int suicides, shotdamage, damage, explosivedamage, hits, misses, shots;
    int captured, stolen, defended;
    
    extplayerdata()
    {
        cn = 0;
        ping = 0;
        name[0] = 0;
        team[0] = 0;
        frags = 0;
        flags = 0;
        deaths = 0;
        teamkills = 0;
        acc = 0;
        health = 0;
        armour = 0;
        gunselect = 0;
        privilege = 0;
        state = 0;
        ip = 0;
        mod = 0;
        suicides = shotdamage = damage = explosivedamage = hits = misses = shots = 0;
        captured = stolen = defended = 0;
        lastseen = totalmillis;
    }
    void reset() {
        cn = 0;
        ping = 0;
        name[0] = 0;
        team[0] = 0;
        frags = 0;
        flags = 0;
        deaths = 0;
        teamkills = 0;
        acc = 0;
        health = 0;
        armour = 0;
        gunselect = 0;
        privilege = 0;
        state = 0;
        ip = 0;
        mod = 0;
        suicides = shotdamage = damage = explosivedamage = hits = misses = shots = 0;
        captured = stolen = defended = 0;
        lastseen = totalmillis;
    }
    void update(const struct extplayerdata &ndata)
    {
        cn = ndata.cn;
        ping = ndata.ping;
        strncpy(name, ndata.name, MAXEXTNAMELENGHT-1);
        name[MAXEXTNAMELENGHT-1] = 0;
        strncpy(team, ndata.team, MAXEXTTEAMLENGHT-1);
        team[MAXEXTTEAMLENGHT-1] = 0;
        frags = ndata.frags;
        flags = ndata.flags;
        deaths = ndata.deaths;
        teamkills = ndata.teamkills;
        acc = ndata.acc;
        health = ndata.health;
        armour = ndata.armour;
        gunselect = ndata.gunselect;
        privilege = ndata.privilege;
        state = ndata.state;
        ip = ndata.ip;
        mod = ndata.mod;
        suicides = ndata.suicides;
        shotdamage = ndata.shotdamage;
        damage = ndata.damage;
        explosivedamage = ndata.explosivedamage;
        hits = ndata.hits;
        misses = ndata.misses;
        shots = ndata.shots;
        captured = ndata.captured;
        stolen = ndata.stolen;
        defended = ndata.defended;
        lastseen = totalmillis;
    }
    extplayerdata(const struct extplayerdata &ndata)
    {
        update(ndata);
    }
};

#define MAXEXTRETRIES 2
#define EXTRETRIESINT 500
#define EXTREFRESHINT 3000

struct extplayerinfo
{
    bool finished;
    int lastattempt;
    int attempts;
    bool needrefresh;
    struct extplayerdata data;
    extplayerinfo()
    {
        finished = false;
        needrefresh = false;
        attempts = 0;
        lastattempt = totalmillis;
    }
    extplayerinfo(bool refresh)
    {
        finished = false;
        needrefresh = refresh;
        attempts = 0;
        lastattempt = totalmillis;
    }
    void resetextdata()
    {
        finished = false;
        attempts = 0;
        lastattempt = totalmillis;
        data.reset();
    }
    void setextplayerinfo()
    {
        attempts = 0;
        finished = true;
    }
    void setextplayerinfo(struct extplayerdata ndata)
    {
        attempts = 0;
        finished = true;
        data.update(ndata);
    }
    void addattempt()
    {
        attempts++;
        lastattempt = totalmillis;
    }
    bool needretry()
    {
        if(needrefresh)
        {
            return lastattempt + EXTREFRESHINT < totalmillis;
        }
        else
        {
            return !finished && attempts <= MAXEXTRETRIES && lastattempt + EXTRETRIESINT < totalmillis;
        }
    }
    bool isfinal()
    {
        return !needrefresh && (finished || attempts > MAXEXTRETRIES);
    }
    int getextplayerinfo()
    {
        if(!finished) return -1;
        return 0;
    }
    int getextplayerinfo(struct extplayerdata &ldata)
    {
        if(!finished) return -1;
        ldata.update(data);
        return 0;
    }
};

#endif // __EXTENDEDINFO_H_
