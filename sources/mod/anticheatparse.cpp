// parse anticheat
#include "engine.h"

namespace game{
  extern const char *getname();
}
namespace mod {
    namespace acc {
      extern ENetPeer *curpeer;
      #define put(n,f) packetbuf n(MAXTRANS); f; enet_peer_send(curpeer, 1, n.finalize())
      
      void parsemessages(ucharbuf &p) {
        static char text[MAXTRANS];
        int type;

        while(p.remaining()) switch(type = getint(p)) {
          case ACC_N_SERVINFO: {
            put(p, {
              putint(p, ACC_N_CONNECT);
              sendstring(game::getname(), p);
              putint(p, PROTOCOL_NUM);
            });
          } break;
          
          case ACC_N_CONNECT: {
            getstring(text, p);
            crowd("%s", text);
          } break;
        }
    }
    
    void serverConnect(uint ip, int port) {
      if(!curpeer) return;
      put(p, {
        putint(p, ACC_N_SERVCONN);
        putint(p, ip);
        putint(p, port);
      });
    }
  }
}