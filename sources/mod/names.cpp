#include "cube.h"
#include "game.h"

namespace mod {
  namespace names {
    hashtable<uint, vector<char *>> namelist;

    void addNames(uint ip, char *name) {
      int nip = ip&0xFFFFFF;
      if(!nip) return;

      bool found = false;
      if(namelist.access(ip)) {
        vector<char *> &nlist = namelist[ip];
        loopv(nlist){
          if(!strcmp(nlist[i], name)){
            nlist.remove(i);
            break;
          }
        }

        nlist.insert(0, newstring(name));
        while(nlist.length()>15) delete[] nlist.pop();
        found = true;
      }

      if(!found) {
        vector<char *> &nlist = namelist[ip];
        nlist.add(newstring(name));
      }
    }

    void addNames(uint ip, vector<char *> names) {
      loopv(names) {
        addNames(ip, names[i]);
      }
    }

    void addName(uint ip, int cn) {
      addNames(ip, game::getclient(cn)->name);
    }

    void addName(uint ip, char *name) {
      addNames(ip, name);
    }

    void addName(const int cn, char *name) {
      addNames(game::getclient(cn)->extdata.data.ip, name);
    }

    ICOMMAND(cname, "ii", (int *cn, int *ret), {
      fpsent *c = game::getclient(*cn);
      if(!c) {
        if(!ret) conoutf("invalid cn: %i", *cn);
        return;
      }
      defformatstring(line)("\f1%s\f0 used the names: \f3", c->name);
      loopvj(namelist[c->extdata.data.ip])
      {
        concatstring(line, namelist[c->extdata.data.ip][j]);
        concatstring(line, " ");
      }
      conoutf("%s", line);
    });

    ICOMMAND(addname, "ss", (const char *ip, const char *names), {
      vector<char *> localnames;
      splitlist(names, localnames);
      addNames(endianswap(GeoIP_addr_to_num(ip)), localnames);
    });

    void savenames(stream *file)
    {
      enumeratekt(namelist, uint, ip, vector<char *>, names, {
        // write name allocator to IP
        char text[MAXTRANS];
        text[0] = '\0';
        loopv(names)
        {
          concatstring(text, names[i]);
          if (i!=names.length()-1) concatstring(text, " ");
        }
        file->printf("addname %s %s", GeoIP_num_to_addr(endianswap(ip)), escapestring(text));
        file->printf("\n");
      });
    }
  }
}
