// anticheat core
#include "engine.h"

namespace mod {
  namespace acc {
    ENetHost *clienthost = NULL;
    ENetPeer *curpeer = NULL, *connpeer = NULL;
    ENetAddress address;
    int connmillis = 0, connattempts = 0, discmillis = 0;

    extern void parsemessages(ucharbuf &p);
    void s2c(int chan, ENetPacket *packet) {  // processes any updates from the server
      if(chan != 1) return;
      packetbuf p(packet);
      if(chan != 1) return;
      if(p.packet->flags&ENET_PACKET_FLAG_UNSEQUENCED) return;
      parsemessages(p);
    }
    
    const char *disconnectreason(int r) {
      switch(r) {
        case ACC_DISC_EOP: return "end of packet";
        case ACC_DISC_MSGERR: return "message error";
        case ACC_DISC_TIMEOUT: return "timeout";
        case ACC_DISC_OVERFLOW: return "packet overflow";
        case ACC_DISC_PROTOCOL: return "invalid protocol";
      }
      return NULL;
    }
    
    void accDeInit() {
      if(curpeer) {
        if(!discmillis) {
          enet_peer_disconnect(curpeer, DISC_NONE);
          enet_host_flush(clienthost);
          discmillis = totalmillis;
        }
        if(curpeer->state!=ENET_PEER_STATE_DISCONNECTED) {
          enet_peer_reset(curpeer);
        }
        curpeer = NULL;
        discmillis = 0;
      }
    }

    #define checkif(a,b) if(a) {fatal(b); exit (EXIT_FAILURE);}
    #define checkiff(a,f) if(a) {f;}
    void accInit() {
      conoutf("[AntiCheat] Connecting ..");
      checkif(enet_initialize () != 0, "Error initializing enet.")
      
      address.host = ENET_HOST_BROADCAST;
      address.port = 1234;
      
      if(!clienthost)  {
        clienthost = enet_host_create(NULL, 2, server::numchannels(), 0, 0);
        checkiff(!clienthost, conoutf("[AntiCheat] \f3Could not connect to server");return)
        clienthost->duplicatePeers = 0;
      }
      
      atexit(enet_deinitialize);
      
      clienthost->noTimeouts = 0;
      connpeer = enet_host_connect(clienthost, &address, server::numchannels(), 0);
      enet_host_flush(clienthost);
      connmillis = totalmillis;
      connattempts = 0;
    }

    void accMainLoop() {
      if(!connpeer && !curpeer) return;
      ENetEvent event;
      if(!clienthost) return;
      if(connpeer && totalmillis/3000 > connmillis/3000) {
        connmillis = totalmillis;
        ++connattempts; 
        if(connattempts > 3) {
          conoutf("[AntiCheat] \f3Could not connect to server");
          connpeer = NULL;
          return;
        }
      }
      
      while(clienthost && enet_host_service(clienthost, &event, 0)>0)
        switch(event.type) {
          case ENET_EVENT_TYPE_CONNECT:
            disconnect(false, false); 
            localdisconnect(false);
            curpeer = connpeer;
            connpeer = NULL;
            break;

          case ENET_EVENT_TYPE_RECEIVE:
            if(discmillis) conoutf("attempting to disconnect...");
            else s2c(event.channelID, event.packet);
            enet_packet_destroy(event.packet);
            break;

          case ENET_EVENT_TYPE_DISCONNECT:
            if(event.data>=DISC_NUM) event.data = DISC_NONE;
            if(event.peer==connpeer) {
              crowd("\f3Could not connect to Crowd Server");
              if(connpeer->state!=ENET_PEER_STATE_DISCONNECTED) enet_peer_reset(connpeer);
              connpeer = NULL;
              enet_host_destroy(clienthost);
              clienthost = NULL;
            } else {
              if(!discmillis || event.data) {
                const char *msg = disconnectreason(event.data);
                if(msg) crowd("\f3Server error, disconnected: %s", msg);
                else crowd("Server error, disconnected.");
              }
            }
            return;

        default:
            break;
      }
    }
  }
}